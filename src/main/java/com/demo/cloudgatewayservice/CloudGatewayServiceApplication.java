package com.demo.cloudgatewayservice;

import java.util.ArrayList;
import java.util.List;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import reactor.core.publisher.Mono;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
@OpenAPIDefinition(info =
@Info(title = "Cloud Gateway", version = "1.0", description = "Documentation cloud API v1.0")
		)
public class CloudGatewayServiceApplication {

	@Autowired
	RouteDefinitionLocator locator;
	
	@Autowired
	private WebClient.Builder webClientBuilder;
	
	public static void main(String[] args) {
		SpringApplication.run(CloudGatewayServiceApplication.class, args);
	}



	@Bean
	public List<GroupedOpenApi> apis() {
		List<GroupedOpenApi> groups = new ArrayList<>();
		List<RouteDefinition> definitions = locator.getRouteDefinitions().collectList().block();
		assert definitions != null;
		definitions.stream().filter(routeDefinition -> routeDefinition.getId().matches(".*-service")).forEach(routeDefinition -> {
			String name = routeDefinition.getId().replaceAll("-service", "");
			groups.add(GroupedOpenApi.builder().pathsToMatch("/" + name + "/**").group(name).build());
		});
		return groups;
	}

	
	
	/*
	 * @GetMapping("/account") public Mono<String> accountSwaggerDoc() { return
	 * webClientBuilder.build().get().uri(
	 * "http://localhost:8084/swagger-doc/v3/api-docs").retrieve().bodyToMono(String
	 * .class); }
	 * 
	 * @GetMapping("/loan") public Mono<String> loanSwaggerDoc() { return
	 * webClientBuilder.build().get().uri(
	 * "http://localhost:8086/swagger-doc/v3/api-docs").retrieve().bodyToMono(String
	 * .class); }
	 */
	@GetMapping("/customer")
	public Mono<String> customerSwaggerDoc() {
		 return webClientBuilder.build().get().uri("http://localhost:8082/v1/swagger-doc/v3/api-docs")
				 .retrieve().bodyToMono(String.class).flatMap(body -> {
				return Mono.just(body.replaceAll("8082", "8081"));
			});

	}
}
