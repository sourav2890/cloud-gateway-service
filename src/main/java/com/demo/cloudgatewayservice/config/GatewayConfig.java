package com.demo.cloudgatewayservice.config;

import com.demo.cloudgatewayservice.filter.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

	@Autowired
	private JwtAuthenticationFilter filter;

	@Bean
	public RouteLocator routes(RouteLocatorBuilder builder) {
		return builder.routes().route("auth", r -> r.path("/v1/auth/**").filters(f -> f.filter(filter)).uri("lb://auth-service"))
				.route("customer", r -> r.path("/v1/customer/**").filters(f -> f.filter(filter)).uri("lb://customer-service"))
				.route("account", r -> r.path("/v1/account/**").filters(f -> f.filter(filter)).uri("lb://account-service")).build();
	}

}
